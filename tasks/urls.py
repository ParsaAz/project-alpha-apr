from django.urls import path
from tasks.views import create_tasks, user_tasks

urlpatterns = [
    path("create/", create_tasks, name="create_task"),
    path("mine/", user_tasks, name="show_my_tasks"),
]
